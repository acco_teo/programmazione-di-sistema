//
//  main.cpp
//  StringBuffer
//
//  Created by Matteo Accornero on 25/03/19.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include <iostream>
#include "StringBuffer.h"
using namespace std;

int main(int argc, const char * argv[]) {
    StringBuffer s1("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ip");
    StringBuffer s2("world!");
    printf("%zu-%zu\n", s1.size​​(), s1.capacity());
    s1.​​append​​(" ");
    printf("%zu-%zu\n", s1.size​​(), s1.capacity());
    s1.​​append​​(s2);
    printf("%zu-%zu\n", s1.size​​(), s1.capacity());
    //printf("%s", s1.​c_str​​());
    s1.​​set​​("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
    s1.​​append​​("\n");
    s2.clear();

    for (int i=0; i<10; i++){
        s2.​​append​​(s1);
        printf("%s", s2.​c_str​​());
        printf("%s", "==================\n");
    }
    printf("%s", "***************************\n");
    printf("%s", s2.​c_str​​()); //Lorem ipsum ... 10 volte
    printf("%zu-%zu", s2.size​​(), s2.capacity()); //1240
}
