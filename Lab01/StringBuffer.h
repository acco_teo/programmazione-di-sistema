//
//  StringBuffer.h
//  StringBuffer
//
//  Created by Matteo Accornero on 25/03/19.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#define BUFLEN 128

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef StringBuffer_h
#define StringBuffer_h

class StringBuffer {
private:
    char* bufptr;
    size_t buflen;
    size_t bufusg;
public:
    StringBuffer();
    StringBuffer(const char* str);
    StringBuffer(const StringBuffer& sb);
    ~StringBuffer();
    StringBuffer& operator= (const StringBuffer& sb);
    size_t size​​();
    size_t capacity();
    void clear();
    void ​insert​​(const char* ​str​, size_t ​pos​);
    void ​insert​​(const StringBuffer& ​sb​, size_t ​pos​);
    void ​​append​​(const char* ​str​);
    void ​​append​​(const StringBuffer& ​sb​);
    const char* ​c_str​​ ();
    void ​​set​​(const char* ​str​);
    void ​​set​​(const StringBuffer& ​s);
};

#endif /* StringBuffer_h */
