//
//  StringBuffer.cpp
//  StringBuffer
//
//  Created by Matteo Accornero on 25/03/19.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include "StringBuffer.h"

StringBuffer::StringBuffer() {
    bufptr = new char[BUFLEN];
    bufptr[0] = '\0';
    buflen = BUFLEN;
    bufusg = 0;
}

StringBuffer::StringBuffer(const char* str) {
    size_t sl = strlen(str);
    buflen = (sl / BUFLEN + 1) * BUFLEN;
    bufptr = new char [buflen];
    bufusg = sl;
    strcpy(bufptr, str);
}

StringBuffer::StringBuffer(const StringBuffer& sb) {
    buflen = sb.buflen;
    bufptr = new char [buflen];
    strcpy(bufptr, sb.bufptr);
    bufusg = sb.bufusg;
}

StringBuffer::~StringBuffer() {
    delete[] bufptr;
}

StringBuffer& StringBuffer::operator= (const StringBuffer &sb) {
    if(this != &sb){
        delete[] bufptr;
        bufptr = nullptr;
        bufusg = sb.bufusg;
        buflen = sb.buflen;
        bufptr = new char [sb.buflen];
        strcpy(bufptr, sb.bufptr);
    }
    return *this;
}

size_t StringBuffer::size​​() {
    return bufusg;
}

size_t StringBuffer::capacity() {
    return this->buflen;
}

void StringBuffer::clear() {
    memcpy(bufptr, "\0", 1);
    bufusg = 0;
}

void StringBuffer::​insert​​(const char* str, size_t pos) {

}

void StringBuffer::​insert​​(const StringBuffer& sb, size_t pos​) {
    this->​insert​​(sb.bufptr, pos​);
}

void StringBuffer::​​append​​(const char* str) {
    if((strlen(str)+bufusg) < buflen) {
        bufptr = strcat(bufptr, str);
        bufusg = strlen(bufptr);
    }
    else {
        char* tmp_bufptr;
        size_t tmp_buflen, tmp_bufusg, sl = strlen(str);
        tmp_bufusg = sl + bufusg;
        tmp_buflen = (tmp_bufusg / BUFLEN + 1) * BUFLEN;
        tmp_bufptr = new char [tmp_buflen];
        memcpy(tmp_bufptr, bufptr, tmp_bufusg+1);
        strcat(tmp_bufptr, str);
        buflen = tmp_buflen;
        bufusg = tmp_bufusg;
        delete[] bufptr;
        bufptr = tmp_bufptr;
    }
}

void StringBuffer::​​append​​(const StringBuffer& sb) {
    ​​append​​((const char*)sb.bufptr);
}

const char* StringBuffer::​c_str​​() {
    return (const char*)bufptr;
}

void StringBuffer::​​set​​(const char* str){
    clear();
    ​​append​​(str);
}

void StringBuffer::​​set​​(const StringBuffer& s) {
    ​​set​​(s.bufptr);
}
