//
// Created by Matteo Accornero on 2019-05-06.
//

#ifndef CRDT_SYMBOL_H
#define CRDT_SYMBOL_H

#include <vector>

class Symbol {
    char c;
    int _charId;
    int _clientId;
    std::vector<int> fracPos;
public:
    Symbol(char c, int charId, int clientId, std::vector<int> fPos);
    bool operator== (const Symbol &s) const;
    char getChar() const;
    int getClientId()const;
    int getCharId() const;
    std::vector<int> getFracPos() const;
};

#endif //CRDT_SYMBOL_H
