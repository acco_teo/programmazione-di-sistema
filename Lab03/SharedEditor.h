//
// Created by Matteo Accornero on 2019-05-06.
//

#ifndef CRDT_SHAREDEDITOR_H
#define CRDT_SHAREDEDITOR_H

#include "NetworkServer.h"
#include "Symbol.h"
#include "Message.h"
#include <string>

class SharedEditor {
    NetworkServer& _server;
    int _siteId;
    std::vector<Symbol> _symbols;
    int _counter;
public:
    explicit SharedEditor(NetworkServer& server) : _server(server){
        this->_siteId = _server.connect(this);
        _counter = 0;
    }
    void localInsert(int index, char value);
    void localErase(int index);
    void process(const Message& m);
    std::string toString();
    int getSiteId();
};

#endif //CRDT_SHAREDEDITOR_H
