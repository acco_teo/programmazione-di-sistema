//
// Created by Matteo Accornero on 2019-05-06.
//

#ifndef CRDT_NETWORKSERVER_H
#define CRDT_NETWORKSERVER_H


#include "Message.h"
#include <vector>
#include <queue>

/* solve the cyclic inclusion of headers */
class SharedEditor;

class NetworkServer {
    std::vector<SharedEditor*> se;
    std::queue<Message> qm;

public:
    NetworkServer();
    int connect(SharedEditor* sharedEditor);
    void disconnect(SharedEditor* sharedEditor);
    void send(const Message& m);
    void dispatchMessages();
};

#endif //CRDT_NETWORKSERVER_H
