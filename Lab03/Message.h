//
// Created by Matteo Accornero on 2019-05-06.
//

#ifndef CRDT_MESSAGE_H
#define CRDT_MESSAGE_H

#include <string>
#include "Symbol.h"

class Message {
    int _siteId;
    int _type;
    Symbol _symbol;
public:
    Message(int t, Symbol symbl, int sId) : _type(t), _symbol(symbl), _siteId(sId) {}
    int getOperationType() const;
    int getSiteId();
    Symbol getSymbol() const;
};

#endif //CRDT_MESSAGE_H
