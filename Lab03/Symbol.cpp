//
// Created by Matteo Accornero on 2019-05-06.
//

#include "Symbol.h"

Symbol::Symbol(char c, int charId, int clientId, std::vector<int> fPos){
    this->c = c;
    this->_charId = charId;
    this->_clientId = clientId;
    this->fracPos = fPos;
}

bool Symbol::operator== (const Symbol &s) const {
    if(this->c == s.getChar() && this->_clientId == s.getClientId() && this->_charId == s.getCharId() && this->getFracPos() == s.getFracPos())
        return true;
    else
        return false;
}

char Symbol::getChar() const {
    return this->c;
}
int Symbol::getClientId() const {
    return this->_clientId;
}
int Symbol::getCharId() const {
    return this->_charId;
}

std::vector<int> Symbol::getFracPos() const {
    return this->fracPos;
}
