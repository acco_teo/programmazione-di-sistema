//
// Created by Matteo Accornero on 2019-05-25.
//

#include "Message.h"
#include "Symbol.h"

int Message::getOperationType() const {
    return _type;
}

Symbol Message::getSymbol() const {
    return _symbol;
}

int Message::getSiteId(){
    return _siteId;
}
