//
// Created by Matteo Accornero on 2019-05-06.
//

#include "SharedEditor.h"
#include "NetworkServer.h"
#include <algorithm>

NetworkServer::NetworkServer() {

}

int NetworkServer::connect(SharedEditor *sharedEditor) {
    se.insert(se.end(), sharedEditor);
    return static_cast<int>(se.size());
}

void NetworkServer::disconnect(SharedEditor *sharedEditor) {
    auto it = std::find(se.begin(), se.end(), sharedEditor);
    se.erase(it, it);
}

void NetworkServer::send(const Message &m) {
    this->qm.push(m);
}

void NetworkServer::dispatchMessages() {
    while(this->qm.size() > 0) {
        for(SharedEditor* e : se) {
            if(e->getSiteId() != this->qm.front().getSiteId())
                e -> process(this->qm.front());
        }
        this->qm.pop();
    }
}