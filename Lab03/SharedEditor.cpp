//
// Created by Matteo Accornero on 2019-05-06.
//

#include "SharedEditor.h"

void SharedEditor::localInsert(int index, char value)
{
    std::vector<int> tmp;
    Symbol *left, *right;

    /* case 1: empty vector */
    if(_symbols.empty()) {
        left  = nullptr;
        right = nullptr;
    }
    /* case 2: insert end */
    else if(index == _symbols.size()) {
        left  = _symbols.data() + index - 1;
        right = nullptr;
    }
    /* case 3: insert head */
    else if(index == 0) {
        left  = nullptr;
        right = _symbols.data() + index;
    }
    /* case 4: generic insert */
    else {
        left  = _symbols.data() + index - 1;
        right = _symbols.data() + index;
    }

    if (right == nullptr) {
        /* managing first insert and insert in last position */
        tmp.push_back(index+1);
    }
    else {
        /* managing insert in first position and generic insert */
        /* right exists for sure, left don't know */
        long lSize = (left != nullptr) ? left->getFracPos().size() : 0;         /* left symbol fractionalIndex length */
        long rSize = right->getFracPos().size();                                /* right symbol fractionalIndex length */

        /* copying the longest fractionalIndex */
        tmp = (rSize > lSize) ? right->getFracPos() : left->getFracPos();

        if(rSize == lSize) {
            /* equal sizes: a new layer of indexes may be generated */
            tmp.back() = (right->getFracPos().back() + left->getFracPos().back()) / 2;
            if(tmp.back() == left->getFracPos().back())
                tmp.push_back(5);
        }
        else
            /* different sizes: for sure there is a free index at the same layer of the longest fractionalIndex */
            tmp.back() = (rSize > lSize) ? (right->getFracPos().back() / 2) : (left->getFracPos().back() + 10) / 2;
    }

    /* creating new Symbol, updating symbolId generator and sending message to Server*/
    Symbol s (value, this->_counter, this->_siteId, tmp);
    this->_counter++;
    _symbols.insert(_symbols.begin()+index, s);
    Message m (0, s, _siteId);
    _server.send(m);

}

void SharedEditor::localErase(int index)
{
    _server.send(Message(1, _symbols[index], _siteId));
    _symbols.erase(_symbols.begin() + index);

}
void SharedEditor::process(const Message& m)
{
    if(m.getOperationType() == 0) {
        /* insert message */
        std::vector<int> tmp = m.getSymbol().getFracPos();
        auto it = std::find_if(_symbols.begin(), _symbols.end(), [tmp](const Symbol &s) -> bool { return tmp < s.getFracPos();});
        if(it != _symbols.end())
            _symbols.insert(it, m.getSymbol());
        else
            _symbols.push_back(m.getSymbol());
    }
    else if (m.getOperationType() == 1) {
        /* erase message */
        auto it = std::find(_symbols.begin(), _symbols.end(), m.getSymbol());
        if(it != _symbols.end()) {
            _symbols.erase(it);
        }
    }
    else {
        /* no valid message, ignore */
        return;
    }
}

std::string SharedEditor::toString()
{
    std::string tmp;
    for(Symbol s : _symbols){
        tmp.push_back(s.getChar());
    }
    return tmp;
}

int SharedEditor::getSiteId()
{
    return this->_siteId;
}