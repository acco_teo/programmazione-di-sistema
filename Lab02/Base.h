//
//  Base.h
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#ifndef BASE_H
#define BASE_H

#include <string.h>
#include <iostream>

class Base {
protected:
    std::string name;
public:
    explicit Base(std::string name) : name(name){}
    std::string getName () const {
        return this->name;
    }
    virtual int mType () const = 0;
    virtual void ls (int indent=0) const = 0;
};

#endif //BASE_H
