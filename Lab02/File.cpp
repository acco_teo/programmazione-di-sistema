//
//  File.cpp
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include "File.h"

int File::mType () const {
    return 2;
}

uintmax_t File::getSize () const{
    return this->byte;
}

void File::ls (int indent) const {
    for(int i=0;i<indent*3;i++)
        std::cout << " ";
    std::cout << this->name << " " << this->byte << std::endl;
}
