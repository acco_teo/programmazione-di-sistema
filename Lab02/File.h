//
//  File.h
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#ifndef FILE_H
#define FILE_H

#include <iostream>
#include "Base.h"

class File : public Base {
private:
    int byte;
public:
    File(std::string name, int dim) : Base(name), byte(dim){}
    uintmax_t getSize () const;
    int mType () const override;
    void ls (int indent=0) const override;
};

#endif //FILE_H
