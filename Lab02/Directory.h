//
//  Directory.h
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include <iostream>
#include <memory>
#include "Base.h"
#include "File.h"
#include <vector>
#include <string>

class Directory : public Base {
private:
    std::vector<std::shared_ptr<Base>>  spset;
    std::weak_ptr<Directory>            parent;
    std::weak_ptr<Directory>            me;
    void                                setMe(std::weak_ptr<Directory> me);
protected:
    explicit Directory(std::string name);
    explicit Directory(std::string name, std::weak_ptr<Directory> parent);
public:
    int                                 mType () const override;
    void                                ls (int indent=0) const override;
    static std::shared_ptr<Directory>   getRoot();
    std::shared_ptr<Directory>          addDirectory(std::string nome);
    std::shared_ptr<File>               addFile(std::string nome, uintmax_t size);
    std::shared_ptr<Base>               get(std::string name);
    std::shared_ptr<Directory>          getDir(std::string name);
    std::shared_ptr<File>               getFile(std::string name);
    void                                remove(std::string nome);
};
