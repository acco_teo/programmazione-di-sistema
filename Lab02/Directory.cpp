//
//  Directory.cpp
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include "Directory.h"
#include <memory>

Directory::Directory(std::string name) : Base(name){}

Directory::Directory(std::string name, std::weak_ptr<Directory> parent): Base(name), parent(parent){}

int Directory::mType () const{
    return 1;
}

void Directory::setMe(std::weak_ptr<Directory> me) {
    this->me = me;
}

std::shared_ptr<Directory> Directory::getRoot (){
    static std::shared_ptr<Directory> root = std::make_shared<Directory>(Directory("/"));
    root->setMe(std::weak_ptr<Directory>(root));
    return root;
}

std::shared_ptr<Directory> Directory::addDirectory (std::string nome) {
    for(auto x : this->spset){
        if(x->getName() == nome)
            throw std::exception();
    }
    std::shared_ptr<Directory> newDirectory = std::shared_ptr<Directory> (new Directory(nome, this->me));
    newDirectory->setMe(newDirectory);
    this->spset.insert(this->spset.end(), newDirectory);

    return newDirectory;
}

std::shared_ptr<File> Directory::addFile (std::string nome, uintmax_t size) {
    for(auto x : this->spset){
        if(x->getName() == nome)
            throw std::exception();
    }
    std::shared_ptr<File> newFile = std::shared_ptr<File> (new File(nome, size));
    this->spset.insert(this->spset.end(), newFile);

    return newFile;
}

std::shared_ptr<Base> Directory::get(std::string name) {
    if(name.compare("..") == 0){
        return static_cast<std::shared_ptr<Base>> (this->parent);
    }
    else if(name.compare(".") == 0) {
        return static_cast<std::shared_ptr<Base>> (this->me);
    }
    else {
        std::shared_ptr<Base> pointer;
        for(auto x : this->spset){
            if(x->getName() == name){
                return std::shared_ptr<Base>(x);
            }
        }
        return std::shared_ptr<Base>(nullptr);
    }
}

std::shared_ptr<Directory> Directory::getDir (std::string name) {
    if(name.compare("..") == 0){
        return std::shared_ptr<Directory>(this->parent);
    }
    else if(name.compare(".") == 0) {
        return std::shared_ptr<Directory>(this->me);
    }
    else {
        for(auto x : this->spset){
            if(x->getName() == name){
                if(x->mType() == 1){
                    return std::dynamic_pointer_cast<Directory>(x);
                }
            }
        }
        return std::shared_ptr<Directory>(nullptr);
    }
}

std::shared_ptr<File> Directory::getFile (std::string name) {
    for(auto x : this->spset) {
        if (x->getName() == name) {
            if (x->mType() == 1) {
                return std::dynamic_pointer_cast<File>(x);
            }
        }
    }
    return std::shared_ptr<File>(nullptr);
}

void Directory::remove (std::string nome) {
    if(nome.compare(".") == 0 || nome.compare("..") == 0){
        throw std::exception();
    }
    else{
        int i =0;
        for(auto x : this->spset){
            i++;
            if(x->getName() == nome)
                this->spset.erase(spset.begin()+i);
        }
    }
}

void Directory::ls (int indent) const {
    for(int i=0;i<indent*3;i++)
        std::cout << " ";
    std::cout << "[+] " + this->name << std::endl;
    for(auto x : this->spset){
            x->ls(indent+1);
    }
}
