//
//  main.cpp
//  FileSystem
//
//  Created by Matteo Accornero on 15/04/2019.
//  Copyright © 2019 Matteo Accornero. All rights reserved.
//

#include <iostream>
#include "./File.h"
#include "./Directory.h"

int main() {
    std::shared_ptr<Directory> root = Directory::getRoot();
    auto alfa = root->addDirectory("alfa");
    alfa->addDirectory("beta")->addFile("beta1", 100);
    alfa->getDir("beta")->addFile("beta2", 200);
    alfa->getDir("..")->ls();
    alfa->remove("beta");
    root->ls();
    return 0;
}
