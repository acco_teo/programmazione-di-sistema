//
// Created by Matteo Accornero on 23/07/19.
//

#include "InputPath.h"

InputPath::InputPath(QWidget *parent) : QWidget(parent)
{
    QStringList wordList;

    inputPath = Model::getModel()->getInputPath();

    lineEdit = new QLineEdit(inputPath, this);

    completer = new QCompleter(this);
    validator = new PathValidator(lineEdit);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setModel(new QDirModel(completer));

    lineEdit->setCompleter(completer);
    lineEdit->setClearButtonEnabled(true);
    lineEdit->setValidator(validator);

    connect(lineEdit,SIGNAL(editingFinished()), this, SLOT(enterPressed()));
    connect(completer,SIGNAL(activated(const QString&)), this, SLOT(completionSelected(const QString&)));
    connect(lineEdit, SIGNAL(inputRejected()), this, SLOT(wrongInput()));

    pushButton = new QPushButton("Up",this);

    layout = new QHBoxLayout(this);
    layout->addWidget(lineEdit);
    layout->addWidget(pushButton);

    this->setLayout(layout);
}

void InputPath::enterPressed()
{
    Model::getModel()->setInputPath(lineEdit->text());
}

void InputPath::completionSelected(const QString& input)
{
    Model::getModel()->setInputPath(input);
}

void InputPath::wrongInput()
{
    qDebug() << "wrong input path inserted by the user.";
}
