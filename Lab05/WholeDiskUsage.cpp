///
// Created by Matteo Accornero on 23/07/19.
//

#include <QtCore/QStorageInfo>
#include <cmath>
#include "WholeDiskUsage.h"

using namespace std;

WholeDiskUsage::WholeDiskUsage(QWidget *parent) : QWidget(parent)
{
    qint64 freeGB = 0;
    qint64 totalGB = 0;
    QStorageInfo* storageInfo = new QStorageInfo("/");

    series = new QHorizontalStackedBarSeries(this);

    freeGB = storageInfo->bytesFree() / pow(2,30);
    totalGB = storageInfo->bytesTotal() / pow(2,30);

    gbTotal = new QBarSet("", this);
    *gbTotal << totalGB;
    QString totalLabel = QString::fromStdString("Total: ").append(QString::fromStdString(to_string(totalGB)));
    gbTotal->setLabel(totalLabel);

    gbUsed = new QBarSet("", this);
    *gbUsed << totalGB-freeGB;
    QString usedLabel = QString::fromStdString("Used: ").append(QString::fromStdString(to_string(totalGB-freeGB)));
    gbUsed->setLabel(usedLabel);

    series->append(gbTotal);
    series->append(gbUsed);

    barChart = new QChart();
    barChart->addSeries(series);
    barChart->setTitle("Root partition usage in GB");
    barChart->setAnimationOptions(QChart::SeriesAnimations);
    
    barChartView = new QChartView(barChart, this);
    barChartView->setRenderHint(QPainter::Antialiasing);

    layout = new QHBoxLayout(this);
    layout->addWidget(barChartView);
    layout->setMargin(0);
    this->setLayout(layout);
    this->setFixedHeight(170);
}
