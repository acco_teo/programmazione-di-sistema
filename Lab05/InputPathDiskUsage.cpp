//
// Created by Matteo Accornero on 23/07/19.
//

#include "InputPathDiskUsage.h"
#include "Model.h"

InputPathDiskUsage::InputPathDiskUsage(QWidget *parent) : QWidget(parent)
{
    series = new QPieSeries(this);

    pieChart = new QChart();
    pieChart->addSeries(series);

    partialTitle = "Disk Usage for: ";
    partialTitle.append(Model::getModel()->getInputPath());

    pieChart->setTitle(partialTitle);
    pieChart->setAnimationOptions(QChart::SeriesAnimations);

    pieChartView = new QChartView(pieChart, this);
    pieChartView->setRenderHint(QPainter::Antialiasing);

    layout = new QHBoxLayout(this);
    layout->addWidget(pieChartView);
    this->setLayout(layout);

    connect(Model::getModel(), SIGNAL(pathChanged(const QString&)), this, SLOT(updatePathDiskUsage(const QString&)));
}

void InputPathDiskUsage::updatePathDiskUsage(const QString& newPath)
{
    pieChart->removeAllSeries();
    series = new QPieSeries(this);

    vector<DirectoryEntry>* entries = Model::getModel()->getDirectoryInfo();
    for(const DirectoryEntry& entry : *entries){
        QPieSlice* slice = new QPieSlice(QString::fromStdString(entry.name),entry.size, series);
        slice->setLabelVisible(true);
        series->append(slice);
    }
    pieChart->addSeries(series);
}
