#include <iostream>
#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QHorizontalStackedBarSeries>
#include <QBarSet>
#include <QChart>
#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include "InputPath.h"
#include "WholeDiskUsage.h"
#include "InputPathDiskUsage.h"
#include "Model.h"

using namespace std;
using namespace QtCharts;

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    QWidget *window = new QWidget();

    Model* model = Model::getModel();
    model->setParent(window);

    WholeDiskUsage* wdu = new WholeDiskUsage(window);

    InputPath* ip = new InputPath(window);

    InputPathDiskUsage* ipdu = new InputPathDiskUsage(window);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    QVBoxLayout *subLayout1 = new QVBoxLayout();
    subLayout1->addWidget(ipdu);
    QHBoxLayout *subLayout2 = new QHBoxLayout();
    subLayout2->addWidget(wdu);
    subLayout2->addWidget(ip);
    mainLayout->addLayout(subLayout2);
    mainLayout->addLayout(subLayout1);

    window->setLayout(mainLayout);

    window->showMaximized();
    window->show();
    return QApplication::exec();
}
