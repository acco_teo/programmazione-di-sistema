//
// Created by Matteo Accornero on 23/07/19.
//

#include <QtCore/QDir>
#include "PathValidator.h"

PathValidator::PathValidator(QObject *parent) : QValidator(parent) {}

QValidator::State PathValidator::validate(QString &path, int &i) const
{
    QDir pathDir(path);

    if(path.endsWith("/")) {
        if(pathDir.exists())
            return Acceptable;
        else return Invalid;
    }

    if(pathDir.exists())
        return Acceptable;
    else return Intermediate;
}
