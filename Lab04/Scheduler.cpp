//
// Created by Matteo Accornero on 30/06/19.
//

#include <iostream>
#include "Scheduler.h"

Scheduler::Scheduler()
{
    this->ready = priority_queue<Job, vector<Job>, GreaterThanByStartTime>();
    this->runnable = queue<Job>();
    this->completed = vector<Job>();
    this->workers = vector<thread>();
}

void Scheduler::submit(Job job)
{
    this->ready.push(job);
}


void Scheduler::start()
{
    int num_workers = thread::hardware_concurrency();
    if(num_workers == 0)
        num_workers = 2;

    for(int i=0; i< num_workers; i++) {
        thread t = thread(&Scheduler::workerExecuteJob, this);
        workers.push_back(std::move(t));
    }

    cout << "Simulation is starting...number of threads created: " << num_workers << endl;

    while(!ready.empty()) {

        Job j = ready.top();

        this_thread::sleep_for(chrono::milliseconds(j.getStart_time()));

        lock_guard<mutex> lg_r(mutex_runnable);
        lock_guard<mutex> lg_cv(mutex_cv);
        runnable.push(j);
        cv_running_not_empty.notify_one();

        ready.pop();
    }
}

void Scheduler::workerExecuteJob()
{
    thread::id tid = this_thread::get_id();

    mutex_runnable.lock();

    while(!runnable.empty() || !ready.empty()) {

        Job j;

        while(runnable.empty()) {
            mutex_runnable.unlock();
            unique_lock<mutex> ul(mutex_cv);
            cv_running_not_empty.wait_for(ul, chrono::duration<int>(1), [this]()->bool{return !runnable.empty();}); //wait and release lock
            mutex_runnable.lock();

            if(runnable.empty() && ready.empty()) {
                mutex_runnable.unlock();
                return;
            }
        }

        j = runnable.front();
        runnable.pop();
        mutex_runnable.unlock();

        int ms = min(j.getDuration()-j.getExecution_time(), time_quantum);
        this_thread::sleep_for(chrono::milliseconds(ms));

        j.setExecution_time(j.getExecution_time() + ms);
        j.setWait_time(0); //TODO

        if(j.getExecution_time() == j.getDuration()) {
            j.setCompletion_time(0);
            lock_guard<mutex> lg(mutex_completed);
            cout << "Thread " << tid << " fully completed the job " << j.getId() << endl;
            completed.push_back(j);
        } else {
            lock_guard<mutex> lg(this->mutex_runnable);
            cout << "Thread " << tid << " worked on the job " << j.getId() << endl;
            runnable.push(j);
            cv_running_not_empty.notify_one();
        }
        mutex_runnable.lock();
    }

    mutex_runnable.unlock();
    cout << "Thread " << tid << " is terminating" << endl;
}


Scheduler::~Scheduler()
{
    for(thread& worker : this->workers){
        worker.join();
    }

    cout << "\n\nScheduler reporting: \n" <<endl;
    for( Job j : this->completed){
        cout << "Job "<< j.getId() << " has been finished." << endl;
    }
    cout << "\n...Scheduler say bye!" <<endl;
}
