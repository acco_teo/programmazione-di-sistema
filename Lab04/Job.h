//
// Created by Matteo Accornero on 30/06/19.
//

#ifndef LAB4_JOB_H
#define LAB4_JOB_H

class Job {
private:
    int id;
    int duration;
    int execution_time;
    int start_time;
    int wait_time;
    int completion_time;
public:
    Job(int id, int start_time, int duration);
    Job();
    int getExecution_time() const;
    void setExecution_time(int execution_time);
    int getWait_time() const;
    void setWait_time(int wait_time);
    int getStart_time() const;
    int getCompletion_time() const;
    void setCompletion_time(int completion_time);
    int getId() const;
    int getDuration() const;
};


struct GreaterThanByStartTime
{
    bool operator()(const Job& j1, const Job& j2) const
    {
        return j1.getStart_time() > j2.getStart_time();
    }
};

#endif //LAB4_JOB_H
